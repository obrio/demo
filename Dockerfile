FROM node:18

EXPOSE 3000
WORKDIR /opt/app

RUN npm install express mysql2
COPY src /opt/app

CMD node main.js