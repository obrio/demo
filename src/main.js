const mysql = require('mysql2');
const express = require('express');

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});

connection.connect();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  connection.query('SELECT * FROM visitors', (error, results) => {
    if (error) throw error;

    let userList = results.map(user => `<li>${user.user}</li>`).join('');
    res.send(`
      <html>
        <head>
          <style>
            body {
              display: flex;
              flex-direction: column;
              align-items: center;
              justify-content: center;
              height: 100vh;
              margin: 0;
            }
          </style>
        </head>
        <body>
          <h1>Visitors List</h1>
          <ul>${userList}</ul>
          <form method="post" action="/add">
            <input type="text" id="newUser" name="newUser" required>
            <button type="submit">I was here</button>
          </form>
        </body>
      </html>
    `);
  });
});

app.post('/add', (req, res) => {
  const newUser = req.body.newUser;

  connection.query('INSERT INTO visitors (user) VALUES (?)', [newUser], (error) => {
    if (error) throw error;
    res.redirect('/');
  });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
